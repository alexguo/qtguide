Qt编程指南
======================
#qtguide

###
准备写一本关于Qt编程的书，读者需要C++基础，推荐《C++ Primer Plus》。
打算写成良心之作，慢慢写吧。
主要是讲传统C++/Qt编程，因为QML我也不会用，而且QML和C++混合编程应该能独立出书的。

###
所用的开发环境可以去Qt官网下载:      
http://download.qt.io/official_releases/qt/5.4/5.4.0/       
也可以从百度网盘下载:       
http://pan.baidu.com/s/1nt3d0jZ     

### 
采用Qt 5.4.0版本开发环境，安装和配置教程上面百度网盘链接里面有，
书籍里面以后也会讲。

###
任重道远，得想想从哪开始。